package com.sagara.dicodingarsenal.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sagara.dicodingarsenal.R
import com.sagara.dicodingarsenal.data.model.Player
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_row_player.view.*
import org.jetbrains.anko.AnkoContext
import timber.log.Timber

class PlayerAdapter(private val list: List<Player>): RecyclerView.Adapter<PlayerAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
/*        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_player, parent, false)
        return ViewHolder(view)*/
        return ViewHolder(ViewPlayerAdapter().createView(AnkoContext.create(parent.context, parent)))
    }

    private val clickSubject = PublishSubject.create<Player>()
    val clickEvent: Observable<Player> = clickSubject

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bind(player: Player){
            itemView.setOnClickListener {
                clickSubject.onNext(player)
            }

            itemView.tv_item_name.text = player.name
            itemView.tv_item_height.text = player.height
            itemView.tv_item_location.text = player.birthLocation
            itemView.tv_item_nationality.text = player.nationality
            itemView.tv_item_position.text = player.position

            Glide.with(itemView.context)
                .load(player.previewPicture)
                .into(itemView.img_item_photo)
        }
    }
}
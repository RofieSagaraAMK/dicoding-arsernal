package com.sagara.dicodingarsenal.adapter

import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.sagara.dicodingarsenal.R
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class ViewPlayerAdapter: AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui){
        cardView {
            id = R.id.card_view
            layoutParams = ViewGroup
                .MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                .apply {
                setMargins(dip(8), dip(4), dip(8), dip(4))
            }
            foregroundGravity = Gravity.CENTER
            radius = 4F
            //card_view:cardCornerRadius = 4dp //not support attribute
            relativeLayout {
                padding = dip(8)
                imageView {
                    id = R.id.img_item_photo
                    scaleType = ImageView.ScaleType.CENTER_CROP
                    //tools:src = @color/colorAccent //not support attribute
                }.lparams(width = dip(130), height = dip(200)) {
                    bottomMargin = dip(4)
                }
                textView {
                    id = R.id.tv_item_name
                    //android:layout_toEndOf = @id/img_item_photo //not support attribute
                    textSize = 16f //sp
                    setTypeface(typeface, Typeface.BOLD)
                    //tools:text = Player Name //not support attribute
                }.lparams(width = matchParent) {
                    leftMargin = dip(16) //in
                    topMargin = dip(16)  //in
                    rightMargin = dip(16)  //in
                    bottomMargin = dip(8)
                    rightOf(R.id.img_item_photo)
                }
                tableLayout {
                    //android:layout_toEndOf = @id/img_item_photo //not support attribute
                    tableRow {
                        themedTextView(R.style.TextSpec_Field) {
                            //style = @style/TextSpec.Field //not support attribute
                            text = resources.getString(R.string.birth_location)
                        }
                        textView {
                            id = R.id.tv_item_location
                            //style = @style/TextSpec.Value //not support attribute
                            //tools:text = Pamplona, Spain //not support attribute
                        }
                    }.lparams(width = matchParent) {
                        bottomMargin = dip(8)
                    }
                    tableRow {
                        themedTextView(R.style.TextSpec_Field) {
                            //style = @style/TextSpec.Field //not support attribute
                            text = resources.getString(R.string.height)
                        }
                        textView {
                            id = R.id.tv_item_height
                            //style = @style/TextSpec.Value //not support attribute
                            //tools:text = 1.78 m (5 ft 10 in) //not support attribute
                        }
                    }.lparams(width = matchParent) {
                        bottomMargin = dip(8)
                    }
                    tableRow {
                        themedTextView(R.style.TextSpec_Field) {
                            //style = @style/TextSpec.Field //not support attribute
                            text = resources.getString(R.string.nationality)
                        }
                        textView {
                            id = R.id.tv_item_nationality
                            //style = @style/TextSpec.Value //not support attribute
                            //tools:text = Spain //not support attribute
                        }
                    }.lparams(width = matchParent) {
                        bottomMargin = dip(8)
                    }
                    tableRow {
                        themedTextView(R.style.TextSpec_Field) {
                            //style = @style/TextSpec.Field //not support attribute
                            text = resources.getString(R.string.position)
                        }
                        textView {
                            id = R.id.tv_item_position
                            //style = @style/TextSpec.Value //not support attribute
                            //tools:text = Defender //not support attribute
                        }
                    }.lparams(width = matchParent) {
                        bottomMargin = dip(8)
                    }
                }.lparams(width = matchParent, height = matchParent) {
                    bottomMargin = dip(4)
                    below(R.id.tv_item_name)
                    leftMargin = dip(16)  //in
                    rightMargin = dip(16)  //in
                    rightOf(R.id.img_item_photo)
                }
            }.lparams(width = matchParent, height = dip(200))
        }
    }
}
package com.sagara.dicodingarsenal.di.component

import androidx.appcompat.app.AppCompatActivity
import com.sagara.dicodingarsenal.ui.MainActivity
import com.sagara.dicodingarsenal.di.module.NetworkModule
import com.sagara.dicodingarsenal.di.module.ThreadModule
import dagger.Component

@Component(modules = [NetworkModule::class, ThreadModule::class])
interface ApplicationComponent {
    fun inject(app: AppCompatActivity)
    fun inject(app: MainActivity)
}
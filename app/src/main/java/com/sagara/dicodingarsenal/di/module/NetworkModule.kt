package com.sagara.dicodingarsenal.di.module

import com.sagara.dicodingarsenal.data.rest.RepoService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
open class NetworkModule {
    private val baseUrl = "https://www.thesportsdb.com/api/v1/json/1/"

    @Provides
    fun provideRetrofit(): Retrofit{
        return Retrofit.Builder().baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun provideRetrofitService(retrofit: Retrofit): RepoService{
        return retrofit.create(RepoService::class.java)
    }
}
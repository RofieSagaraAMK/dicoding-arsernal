package com.sagara.dicodingarsenal.di.module

import com.sagara.dicodingarsenal.utils.DisposableManager
import com.sagara.dicodingarsenal.utils.IDisposableManager
import dagger.Module
import dagger.Provides

@Module
open class ThreadModule {

    @Provides
    fun provideDisposableManager(): IDisposableManager{
        return DisposableManager()
    }
}
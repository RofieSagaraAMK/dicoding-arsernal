package com.sagara.dicodingarsenal.utils

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class DisposableManager: IDisposableManager {

    private val compositeDisposable = CompositeDisposable()

    override fun add(disposable: Disposable){
        compositeDisposable.add(disposable)
    }

    override fun dispose(){
        compositeDisposable.dispose()
    }
}
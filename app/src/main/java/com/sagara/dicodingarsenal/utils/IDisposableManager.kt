package com.sagara.dicodingarsenal.utils

import io.reactivex.disposables.Disposable

interface IDisposableManager {
    fun add(disposable: Disposable)
    fun dispose()
}
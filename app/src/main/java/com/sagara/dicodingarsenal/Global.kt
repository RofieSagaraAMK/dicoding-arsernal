package com.sagara.dicodingarsenal

class Global {
    companion object{
        const val DEFAULT_ICON_ARSENAL = "https://www.thesportsdb.com/images/media/team/fanart/xyusxr1419347566.jpg"
        const val GIT_LAB_URL = "https://gitlab.com/RofieSagaraAMK/dicoding-arsernal"
    }
}
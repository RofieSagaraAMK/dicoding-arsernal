package com.sagara.dicodingarsenal.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.sagara.dicodingarsenal.Global
import com.sagara.dicodingarsenal.R
import com.sagara.dicodingarsenal.data.model.Player
import kotlinx.android.synthetic.main.activity_details.*
import kotlin.text.StringBuilder

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        (intent.getSerializableExtra(PLAYER) as? Player)?.let {
            bind(it)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun bind(player: Player){
        supportActionBar?.title = player.name

        details_tv_name_side.text = StringBuilder("${player.name} - ${player.side}")
        details_tv_nationality.text = player.nationality
        details_tv_date_born.text = player.dateBorn
        details_tv_date_sign.text = player.dateSigning
        details_tv_wage.text = player.wage
        details_tv_birth_location.text = player.birthLocation
        details_tv_position.text = player.position
        details_tv_height.text = player.height
        details_tv_weight.text = player.weight

        Glide.with(this)
            .load(player.picture ?: Global.DEFAULT_ICON_ARSENAL)
            .centerCrop()
            .into(details_iv_picture)
    }

    companion object{
        const val PLAYER = "player"
    }
}

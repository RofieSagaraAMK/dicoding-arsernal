package com.sagara.dicodingarsenal.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.sagara.dicodingarsenal.R
import com.sagara.dicodingarsenal.adapter.PlayerAdapter
import com.sagara.dicodingarsenal.data.model.Player
import com.sagara.dicodingarsenal.data.rest.RepoService
import com.sagara.dicodingarsenal.di.component.DaggerApplicationComponent
import com.sagara.dicodingarsenal.utils.IDisposableManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var repoService: RepoService
    @Inject lateinit var disposableManager: IDisposableManager
    private val subject = PublishSubject.create<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerApplicationComponent.create().inject(this)

        initView()
        prepare()
        prepareLoadData()
        subject.onNext(1)
    }

    private fun initView(){
        swipe_refresh.setOnRefreshListener {
            subject.onNext(1)
        }
    }

    private fun showLoading(): Observable<Int>{
        return Observable.just(1)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                swipe_refresh.isRefreshing = true
            }
    }

    private fun prepareLoadData() {
        val disposable = subject
            .flatMap {
                showLoading()
            }
            .observeOn(Schedulers.io())
            .switchMap {
                repoService.getPlayers()
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ t ->
                Timber.d("size of data ${t.player.size}")
                swipe_refresh.isRefreshing = false
                rv_player.adapter = PlayerAdapter(t.player).apply {
                    clickEvent.subscribe {
                        onAdapterClick(it)
                    }
                }
            }, { e ->
                Timber.e(e, "Error cause")
                showErrorDialog()
            }, {
                Timber.i("Load data complete!")
            })
        disposableManager.add(disposable)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_about -> {
                startActivity(Intent(this, AboutActivity::class.java))
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showErrorDialog(){
        swipe_refresh.isRefreshing = false
        Snackbar.make(findViewById(android.R.id.content), "Error connection!", Snackbar.LENGTH_INDEFINITE)
            .setAction("Retry") {
                //loadData()
                subject.onNext(1)
            }.show()
    }

    private fun onAdapterClick(item: Player){
        Intent(this, DetailsActivity::class.java).apply {
            putExtra(DetailsActivity.PLAYER, item)
        }.also {
            startActivity(it)
        }
    }

    private fun prepare(){
        rv_player.layoutManager = LinearLayoutManager(this)
    }

    override fun onDestroy() {
        disposableManager.dispose()
        super.onDestroy()
    }
}

package com.sagara.dicodingarsenal.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.sagara.dicodingarsenal.Global
import com.sagara.dicodingarsenal.R
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        initView()

        Glide.with(this)
            .load(ContextCompat.getDrawable(this, R.drawable.rofie_sagara))
            .into(about_tv_picture)
    }

    private fun initView(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = "About"

        about_tv_source_code.apply {
            movementMethod = LinkMovementMethod.getInstance()

            setOnClickListener {
                Intent(Intent.ACTION_VIEW).apply {
                    data = Global.GIT_LAB_URL.toUri()
                }.also {
                    startActivity(it)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}

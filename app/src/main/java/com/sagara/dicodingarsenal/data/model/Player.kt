package com.sagara.dicodingarsenal.data.model

import com.google.gson.annotations.SerializedName
import com.sagara.dicodingarsenal.Global
import java.io.Serializable

data class Player(
    @SerializedName("strPlayer") val name: String = "",
    @SerializedName("strNationality") val nationality: String = "",
    @SerializedName("dateBorn") val dateBorn: String = "",
    @SerializedName("dateSigned") val dateSigning: String = "",
    @SerializedName("strSigning") val signing: String = "",
    @SerializedName("strWage") val wage: String = "",
    @SerializedName("strBirthLocation") val birthLocation: String = "",
    @SerializedName("strDescriptionEN") val description: String = "",
    @SerializedName("strSide") val side: String = "",
    @SerializedName("strPosition") val position: String = "",
    @SerializedName("strHeight") val height: String = "",
    @SerializedName("strWeight") val weight: String = "",
    @SerializedName("strThumb") val picture: String? = ""
): Serializable {
    val previewPicture: String
        get() = if (picture != null) "$picture/preview" else Global.DEFAULT_ICON_ARSENAL
}
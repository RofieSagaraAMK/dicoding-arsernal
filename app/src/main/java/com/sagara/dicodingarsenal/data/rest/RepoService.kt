package com.sagara.dicodingarsenal.data.rest

import com.sagara.dicodingarsenal.data.model.Data
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RepoService {

    @GET("searchplayers.php")
    fun getPlayers(@Query("t") team: String = "Arsenal"): Observable<Data>
}